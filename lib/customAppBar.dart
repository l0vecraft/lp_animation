import 'package:flutter/material.dart';
import 'package:lp_animation/album_class.dart';

class CustomAppBar extends SliverPersistentHeaderDelegate {
  double _maxExtend = 350;
  double _minExtend = 100;
  double _maxHeightImage = 200;
  double _minHeightImage = 80;
  double _maxHeigVinyl = 210;
  double _minHeigVinyl = 60;
  double _maxSizeTitleFont = 20;
  double _minSizeTitleFont = 20;
  double _maxSizeSubTitleFont = 0;
  double _minSizeSubTitleFont = 0;

  @override
  double get maxExtent => _maxExtend;

  @override
  double get minExtent => _minExtend;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    final double percent = shrinkOffset / _maxExtend;
    final Size size = MediaQuery.of(context).size;

    return Container(
      color: Color(0xffececea),
      child: Stack(
        children: [
          Positioned(
            bottom: (size.height * .001 * (1 - percent)),
            left: (size.width * .4 * (1 - percent)),
            child: Transform.rotate(
              angle: 5 / (1 - percent),
              child: Image.asset(
                'assets/images/vinyl.png',
                height: (_maxHeigVinyl * (1 - percent))
                    .clamp(_minHeigVinyl, _maxHeigVinyl),
              ),
            ),
          ),
          Positioned(
              bottom: size.height / 85 * (1 - percent),
              left: size.width / 30 * (1 - percent),
              child: Image.asset(
                albumCover.image,
                height: (_maxHeightImage * (1 - percent))
                    .clamp(_minHeightImage, _maxHeightImage),
              )),
          Positioned(
              top: size.height / 10.5 * (1 - percent * .5),
              left: size.width / 3 * (1 - percent * .3),
              child: Column(
                children: [
                  Text(
                    albumCover.subtitle,
                    style: TextStyle(
                        fontSize: _maxSizeTitleFont,
                        fontWeight: FontWeight.w900,
                        letterSpacing: 1.4),
                  ),
                  Text(albumCover.name,
                      style: TextStyle(
                        color: Colors.grey[600],
                      ))
                ],
              ))
        ],
      ),
    );
  }
}
