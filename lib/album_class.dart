class Album {
  String name;
  String description;
  String image;
  String subtitle;

  Album({
    this.name,
    this.description,
    this.image = 'assets/images/cover1.jpg',
    this.subtitle,
  });
}

Album albumCover = Album(
    name: 'The Highlights',
    description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tincidunt ligula ut dolor varius, id ullamcorper ante feugiat. Sed lacinia mi quis leo hendrerit tempus sed facilisis lectus. In id luctus enim, eget suscipit ipsum.\n Quisque aliquet mauris quis mauris feugiat, at laoreet ex tempor. Vestibulum varius mauris id libero laoreet luctus. Curabitur ac arcu at risus scelerisque cursus. Quisque a ligula quam. Ut non felis massa. Cras rhoncus diam at metus vestibulum ultrices.\n Nullam rutrum turpis sit amet porta placerat. Donec finibus id ligula at posuere. Nullam eget rutrum purus, non placerat arcu. Proin sed fermentum ipsum.Nam vel dui ac risus accumsan feugiat sed non dui. Nunc consectetur lorem sed dolor egestas consequat. Nullam in blandit odio, finibus tincidunt augue. Donec ligula lectus, venenatis ut tortor et, ultrices scelerisque urna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi consequat sit amet enim vel fringilla. Fusce placerat laoreet laoreet. Sed nec tempor justo, a bibendum eros.Curabitur a mollis eros. Nam vel tellus congue, rhoncus purus et, gravida odio. Suspendisse vestibulum, metus vel auctor tempor, orci ex faucibus sem, sit amet viverra augue nulla ac quam. Aliquam gravida est sit amet turpis euismod luctus. Proin vitae efficitur dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec pharetra volutpat lectus commodo convallis. Nullam blandit semper nisi, et consectetur tortor commodo in.Proin sed fermentum ipsum.Nam vel dui ac risus accumsan feugiat sed non dui. Nunc consectetur lorem sed dolor egestas consequat. Nullam in blandit odio, finibus tincidunt augue. Donec ligula lectus, venenatis ut tortor et, ultrices scelerisque urna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi consequat sit amet enim vel fringilla. Fusce placerat laoreet laoreet. Sed nec tempor justo, a bibendum eros.Curabitur a mollis eros. Nam vel tellus congue, rhoncus purus et, gravida odio. Suspendisse vestibulum, metus vel auctor tempor, orci ex faucibus sem, sit amet viverra augue nulla ac quam. Aliquam gravida est sit amet turpis euismod luctus. Proin vitae efficitur dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec pharetra volutpat lectus commodo convallis. Nullam blandit semper nisi, et consectetur tortor commodo in.Proin sed fermentum ipsum.Nam vel dui ac risus accumsan feugiat sed non dui. Nunc consectetur lorem sed dolor egestas consequat. Nullam in blandit odio, finibus tincidunt augue. Donec ligula lectus, venenatis ut tortor et, ultrices scelerisque urna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi consequat sit amet enim vel fringilla. Fusce placerat laoreet laoreet. Sed nec tempor justo, a bibendum eros.Curabitur a mollis eros. Nam vel tellus congue, rhoncus purus et, gravida odio. Suspendisse vestibulum, metus vel auctor tempor, orci ex faucibus sem, sit amet viverra augue nulla ac quam. Aliquam gravida est sit amet turpis euismod luctus. Proin vitae efficitur dui. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec pharetra volutpat lectus commodo convallis. Nullam blandit semper nisi, et consectetur tortor commodo in.',
    subtitle: 'The Weeknd');
